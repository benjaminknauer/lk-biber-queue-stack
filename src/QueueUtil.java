public class QueueUtil {

    /**
     * Gibt eine invertierte Queue zurück.
     * Leer die übergebene Queue dabei!
     *
     * @param pQueue zu invertierender Queue, wird geleert (mutiert)
     * @param <T>    Datentyp der Queue
     * @return neue Queue mit invertierter Reihenfolge
     */
    public static <T> Queue<T> invert(Queue<T> pQueue) {
        var temp = new Stack<T>();
        var invertedQueue = new Queue<T>();
        while (!pQueue.isEmpty()) {
            temp.push(pQueue.front());
            pQueue.dequeue();
        }
        while (!temp.isEmpty()) {
            invertedQueue.enqueue(temp.top());
            temp.pop();
        }

        return invertedQueue;
    }

    /**
     * Gibt eine invertierte Queue zurück.
     *
     * @param pQueue zu invertierende Queue, wird invertiert (mutiert)
     * @param <T>    Datentyp der Queue
     */
    public static <T> void invertInplace(Queue<T> pQueue) {
        var temp = new Stack<T>();
        while (!pQueue.isEmpty()) {
            temp.push(pQueue.front());
            pQueue.dequeue();
        }
        while (!temp.isEmpty()) {
            pQueue.enqueue(temp.top());
            temp.pop();
        }
    }

}
