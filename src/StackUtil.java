public class StackUtil {

    /**
     * Gibt einen invertierten Stack zurück.
     * Leer den übergebenen Stack dabei!
     *
     * @param pStack zu invertierender Stack, wird geleert (mutiert)
     * @param <T>    Datentyp des Stacks
     * @return neuer Stack mit invertierter Reihenfolge
     */
    public static <T> Stack<T> invert(Stack<T> pStack) {
        var temp = new Stack<T>();
        while (!pStack.isEmpty()) {
            temp.push(pStack.top());
            pStack.pop();
        }

        return temp;
    }

    /**
     * Invertiert den übergebenen Stack.
     *
     * @param pStack zu invertierender Stack, wird invertiert (mutiert)
     * @param <T>    Datentyp des Stacks
     */
    public static <T> void invertInplace(Stack<T> pStack) {
        var tempInverted = new Stack<T>();
        while (!pStack.isEmpty()) {
            tempInverted.push(pStack.top());
            pStack.pop();
        }
        var tempOriginalOrder = new Stack<T>();
        while (!tempInverted.isEmpty()) {
            tempOriginalOrder.push(pStack.top());
            tempInverted.pop();
        }
        while (!tempOriginalOrder.isEmpty()) {
            pStack.push(pStack.top());
            tempOriginalOrder.pop();
        }
    }

}
