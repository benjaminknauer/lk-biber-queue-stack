public class BiberKueche {
    private Stack<Teller> tellerstapel;
    private Queue<Biber> warteschlange;

    public BiberKueche() {
        tellerstapel = new Stack<Teller>();
        warteschlange = new Queue<Biber>();
        tellerstapelFuellen();
        warteschlangeFuellen();
    }

    private void warteschlangeFuellen() {
        warteschlange.enqueue(new KleinerBiber("Biber 1"));
        warteschlange.enqueue(new GrosserBiber("Biber 2"));
        warteschlange.enqueue(new KleinerBiber("Biber 3"));
        warteschlange.enqueue(new GrosserBiber("Biber 4"));
        warteschlange.enqueue(new KleinerBiber("Biber 5"));
        warteschlange.enqueue(new GrosserBiber("Biber 6"));
    }

    private void tellerstapelFuellen() {
        tellerstapel.push(new GrosserTeller("rot"));
        tellerstapel.push(new KleinerTeller("orange"));
        tellerstapel.push(new GrosserTeller("gelb"));
        tellerstapel.push(new KleinerTeller("grün"));
        tellerstapel.push(new GrosserTeller("blau"));
        tellerstapel.push(new KleinerTeller("lila"));
    }

    /**
     * Prüft, ob Teller auf dem Stapel zu den Bibern in der Warteschlange passen.
     * Gibt entsprechende Meldungen auf der Console aus.
     * Leert dabei die Datenstrukturen.
     */
    public void testeEssensausgabeRekursiv() {
        boolean isNaechsteElementePassenZusammen = isBiberUndTellerPassend(warteschlange.front(), tellerstapel.top());
        if (!isNaechsteElementePassenZusammen) {
            System.out.println("Elemente passen nicht zusammen");
            return;
        }
        warteschlange.dequeue();
        tellerstapel.pop();
        if (warteschlange.isEmpty() && tellerstapel.isEmpty()) {
            System.out.println("Alle Biber bekommen den richtigen Teller");
            return;
        }
        if (warteschlange.isEmpty() || tellerstapel.isEmpty()) {
            System.out.println("Unterschiedliche viele Teller und Biber");
            return;
        }
        testeEssensausgabeRekursiv();
    }

    /**
     * Prüft, ob Teller auf dem Stapel zu den Bibern in der Warteschlange passen.
     * Gibt entsprechende Meldungen auf der Console aus.
     * Stellt den ursprünglichen Zustand der Datenstrukturen wieder her.
     */
    public void testeEssensausgabeIterativ() {
        // Hilfsvariablen, um den Zustand wiederherzustellen.
        Stack<Teller> tempTellerstapel = new Stack<Teller>();
        Queue<Biber> tempWarteschlange = new Queue<Biber>();
        while (!warteschlange.isEmpty() || !tellerstapel.isEmpty()) {
            if (warteschlange.isEmpty() || tellerstapel.isEmpty()) {
                System.out.println("Unterschiedliche viele Teller und Biber");
                return;
            }
            if (!isBiberUndTellerPassend(warteschlange.front(), tellerstapel.top())) {
                System.out.println("Elemente passen nicht zusammen");
                return;
            }
            // um den Zustand wiederherzustellen
            tempTellerstapel.push(tellerstapel.top());
            tempWarteschlange.enqueue(warteschlange.front());
            // Die Elemente passen zusammen. Entfernen und weiter ...
            tellerstapel.pop();
            warteschlange.dequeue();
        }
        // ursprünglichen Zustand wiederherstellen
        tellerstapel = StackUtil.invert(tempTellerstapel);
        warteschlange = QueueUtil.invert(tempWarteschlange);

        System.out.println("Alle Biber bekommen den richtigen Teller");
    }

    /**
     * Prüft, ob die Subtypen von Biber und Teller zusammenpassen.
     *
     * @param pBiber  Zu prüfender Biber.
     * @param pTeller Zu prüfender Teller.
     * @return true, falls Biber und Teller zusammenpassen.
     */
    private boolean isBiberUndTellerPassend(Biber pBiber, Teller pTeller) {
        return pBiber instanceof KleinerBiber && pTeller instanceof KleinerTeller
                || pBiber instanceof GrosserBiber && pTeller instanceof GrosserTeller;
    }

    public static void main(String[] args) {
        BiberKueche meineBiberKueche = new BiberKueche();
        meineBiberKueche.testeEssensausgabeIterativ();
    }

}
